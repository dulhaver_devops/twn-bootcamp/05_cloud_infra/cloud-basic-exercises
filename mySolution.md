## how I solved these exercises

### EXERCISE 0: Clone Git Repository

1. clone the Repository
   - `git clone https://gitlab.com/twn-devops-bootcamp/latest/05-cloud/cloud-basics-exercises`
   - `cd cloud-basic-exercises`
1. create my own branch
   `git checkout -b my-solutions`
1. add my own remote and push everything to into
   - `git remote add dulhaver git@gitlab.com:dulhaver_devops/twn-bootcamp/05_cloud_infra/cloud-basic-exercises`
   - `git push --all dulhaver`

### EXERCISE 1: Package NodeJS App

1. create a local incus(*) debian 12 container
   - `incus launch images:debian/12 bootcamp-04`
1. copy the `app` folder into the container
   - `incus file push -r app bootcamp-04/opt/`
1. copy install_node_from_sousrces script into the container & run it
   - `incus file push ../../04_build-tools/05_sidecar/nodejs_install_from_node_sources.sh bootcamp-04/opt/`
   - `incus exec bootcamp-04 -- bash /opt/nodejs_install_from_node_sources.sh`
1. copy the `app` folder to the incus container
   - `incus file push -r app bootcamp-04/opt/`
1. package the app inside the container
   - `incus exec bootcamp-04 -- bash`
     - `cd /opt/app && npm pack`


### EXERCISE 2: Create a new server
1. create the a new Digital Ocean debian 12 droplet and install jdk-8
   - `cd cd ../terraform-digitalocean/`
   - `tofu apply`

see [terraform-digitalocean](https://gitlab.com/dulhaver_devops/twn-bootcamp/05_cloud_infra/terraform-digitalocean)

### EXERCISE 3: Prepare server to run Node App
1. java installation has been done with the above step
1. copy the [nodejs_installation script](https://gitlab.com/dulhaver_devops/twn-bootcamp/04_build-tools/05_sidecar/-/blob/main/nodejs_install_from_node_sources.sh?ref_type=heads) to the server and install node
   - `scp -i ~/.ssh/id_rsa_twn ../../04_build-tools/05_sidecar/nodejs_install_from_node_sources.sh sysop@209.38.196.119:/home/sysop/`
   - `ssh -i ~/.ssh/id_rsa_twn sysop@209.38.196.119 'bash /home/sysop/nodejs_install_from_node_sources.sh'` 

### EXERCISE 4: Copy App
1. copy my private key into the incus container
   - `incus file push ~/.ssh/id_rsa_twn bootcamp-04/home/.ssh/`
1. enter the local incus build container and push the package to the new server
   - `incus exec bootcamp-04 bash`
     - `cd /opt/app`
     - `root@bootcamp-04:/opt/app# scp -i /root/.ssh/id_rsa_twn /opt/app/bootcamp-node-project-1.0.0.tgz sysop@209.38.196.119:/home/sysop/


### EXERCISE 5: Run Node App
1. ssh into the server and start the app
   - `ssh -i ~/.ssh/id_rsa_twn sysop@209.38.196.119`
     - `cd package`
     - `npm install`
     - `java server.js &`

```
sysop@deb12:~$ ss -tulpn | grep node
tcp   LISTEN 0      511                *:3000            *:*    users:(("node",pid=7928,fd=19))`
```

### EXERCISE 6: Access from browser - configure firewall
![image](./running_app.png)
- firewall configuration is apparently not needed
- The app surprisingly is accessible without specificly opening port 3000 on the droplet
